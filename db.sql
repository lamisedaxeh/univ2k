use data;
/*Table utilsiateur étudiant, professeur, secrétaire, administrateur, etc */
CREATE OR REPLACE TABLE users(
	idUser			int(10) PRIMARY KEY  AUTO_INCREMENT NOT NULL,
	userType		int(1),/*1 2 3*/
	prenom			VARCHAR(20),
	nom				VARCHAR(20),
	tel				VARCHAR(20),/*supporte le +33 et num americain*/
	fonction 		VARCHAR(15)/*Vacataire, ATER, MdC,Professeur*/
);

/*Création d'un cours*/
CREATE OR REPLACE TABLE cours(
	idCour			int(10) PRIMARY KEY  AUTO_INCREMENT NOT NULL, 
	titre 			VARCHAR(50),
	description 	VARCHAR(300),
	heures			TIMESTAMP
);


/*Un cours peut avoir plusieurs matiéres*/
CREATE OR REPLACE TABLE matieres(
	idMat			int(10) PRIMARY KEY  AUTO_INCREMENT NOT NULL, 
	idUser			int(10),
	idCour			int(10),
	semestre		int(4),
	annee			int(4),
	FOREIGN KEY `fk_matieres_user` (idUser) REFERENCES users (idUser),
	FOREIGN KEY `fk_matieres_cour`(idCour) REFERENCES cours (idCour)
);

/***********************
 * Séance de cours
 ***********************/
CREATE OR REPLACE TABLE seance(
	idSeance		int(10) PRIMARY KEY  AUTO_INCREMENT NOT NULL,
	idMat			int(10),
	durée			int(10),/*Durée en minute*/
	description 	VARCHAR(300),
	salle			VARCHAR(20),
	typeCours		VARCHAR(2),/*CM,TP,TD*/
	heures			TIMESTAMP,
	FOREIGN KEY `fk_seance_cour` (idMat) REFERENCES matieres(idMat)
);


/*Note examen + note exo + */
CREATE OR REPLACE TABLE note(
	idNote			int(10) PRIMARY KEY  AUTO_INCREMENT NOT NULL,
	idUser			int(10),
	idMat			int(10),
	note			int(2),
	coef			int(10),
	typeNote		VARCHAR(300),
	description 	VARCHAR(300),
	FOREIGN KEY `fk_note_user`  (idUser)    REFERENCES users    (idUser),
	FOREIGN KEY `fk_note_mat`   (idMat)     REFERENCES matieres (idMat)
);


/*Inscription cours */
CREATE OR REPLACE TABLE inscCours(
	idUser			int(10) NOT NULL,
	idMat			int(10) NOT NULL,
	CONSTRAINT  `pk_insc` PRIMARY KEY (idUser, idMat),
	FOREIGN KEY `fk_insc_user` (idUser) REFERENCES users (idUser),
	FOREIGN KEY `fk_insc_mat`  (idMat)  REFERENCES matieres (idMat)
);

/*Rendu de plusieurs exo */
CREATE OR REPLACE TABLE exo(
	idExo			int(10) PRIMARY KEY  AUTO_INCREMENT NOT NULL,
	idUser			int(10) NOT NULL,
	idMat			int(10) NOT NULL,
	exo				MEDIUMTEXT,/*16M*/
	FOREIGN KEY `fk_exo_user` (idUser) REFERENCES users (idUser),
	FOREIGN KEY `fk_exo_mat`  (idMat)  REFERENCES matieres (idMat)
);

/**
 * Forum
 */
CREATE OR REPLACE TABLE message(
	idMsg			int(10) PRIMARY KEY  AUTO_INCREMENT NOT NULL,
	idUser			int(10) NOT NULL,
	idSeance		int(10) NOT NULL,
	msg				TEXT,/*64kb*/
	FOREIGN KEY `fk_msg_user`   (idUser)    REFERENCES users    (idUser),
	FOREIGN KEY `fk_msg_seance` (idSeance)  REFERENCES seance   (idSeance)
);
