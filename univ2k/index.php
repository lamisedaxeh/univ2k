<?php 
//Some flag
$showAlert = false;  
$showError = false;  
$exists=false; 
//Include connection to BDD
include 'dbconnect.php';    



	$sql = "SELECT m.idUser, COUNT(*)as nbq,u.* from message m,users u where m.idUser=u.idUser GROUP BY m.idUser ORDER BY COUNT(*) DESC;";
	$users = mysqli_query($conn, $sql); 
	$count = mysqli_num_rows($users);  
	if($count < 1){
	echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"> Error.
	<button type="button" class="close" 
	data-dismiss="alert" aria-label="Close">  
	<span aria-hidden="true">×</span>  
	</button></div>';
	}
?>

<!doctype html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8">  
    <meta name="viewport" content="width=device-width, initial-scale=1,shrink-to-fit=no"> 
    <!-- Bootstrap CSS -->  
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

</head> 
<body style="margin:40px"> 
<!--
#################################
# Formulaire get questions
#################################
-->
<div class="container my-4 "> 

    <h1 class="text-center">Voir les questions de l'étudiant :</h1>  
    <form class="row row-cols-lg-auto g-3 align-items-center" id="formEtu" action="questionEtud.php" method="post"> 
	<div class="col">  
	    <label for="num">Numéro étudiant :</label>  
	</div>
	<div class="col">  
	    <input id="num" type="alpha" class="form-control" id="num"
		name="num">     
	</div>
	<div class="col">  
	    <button type="submit" class="btn btn-primary"> 
	    Afficher
	</button>  
	</div>
    </form>  
</div> 

<script>
    function openEtu(n){
	document.getElementById('num').value = n;
	document.getElementById('formEtu').submit();
    }
    function search() {
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("search");
      filter = input.value.toUpperCase();
      table = document.getElementById("userInfo");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
	td = tr[i].getElementsByTagName("td")[0];
	 if (td) {
	  txtValue=tr[i].textContent ;
	  if (txtValue.toUpperCase().indexOf(filter) > -1) {
	    tr[i].style.display = "";
	  } else {
	    tr[i].style.display = "none";
	  }
	}
      }
    }
</script>

<div class="input-group mb-3">
    <button class="btn btn-primary" type="button" id="button-addon1" onclick="search()">Search</button>
    <input class="form-control" type="text" id="search" onkeyup="search()" placeholder="Search for info.." title="Type in a name">
</div>

<table id="userInfo" class="table table-striped table-hover">
      <thead>
    <tr>
      <th scope="col">NB questions</th>
      <th scope="col">Nom</th>
      <th scope="col">Prénom</th>
      <th scope="col">Num étu</th>
      <th scope="col">Tel</th>
    </tr>
  </thead>
  <tbody>
<?php 
    while ($u = mysqli_fetch_assoc($users)){
	echo '<tr onclick="openEtu('.$u['idUser'].')">';
	    echo '<th scope="row">'.$u['nbq'].'</th>';
            echo '<td>'.$u['nom'].'</td>';
            echo '<td>'.$u['prenom'].'</td>';
            echo '<td>'.$u['idUser'].'</td>';
            echo '<td>'.$u['tel'].'</td>';
        echo '</tr>';
    }
?>
 </tbody>
</table>


<!-- JavaScript Lib for bootstrap -->  
<!-- At the end of page for loading optimisation -->

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"> 
</script> 

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"> 
</script> 

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"> 
</script>  
</body>  
</html> 

