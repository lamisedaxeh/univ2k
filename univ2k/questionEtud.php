<?php 
//Some flag
$showAlert = false;  
$showError = false;  
$exists=false; 
//Include connection to BDD
include 'dbconnect.php';    

//Processing post data and save to BDD
if($_SERVER["REQUEST_METHOD"] == "POST") { 
    $id = $_POST["num"];  

    //ESCAPE num and duree
    $id = $conn->real_escape_string($id);

    if(is_numeric($id)){
	$sql = "SELECT * FROM users WHERE idUser = $id;";
	$userInfo = mysqli_query($conn, $sql); 
	$count1 = mysqli_num_rows($userInfo);  
    if($count1 < 1){
	    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"> Error invalid student number.
	    <button type="button" class="btn-close" 
	    data-dismiss="alert" aria-label="Close">  
	    </button></div>';
        echo'<h2 class="text-muted">Aucun utilisateur !</h2>';
    }
	$sql = "SELECT * FROM message m,users u,seance s,matieres m2,cours c WHERE m2.idCour=c.idCour and s.idMat=m2.idMat and m.idSeance =s.idseance and u.idUser=m.idUser and m.idUser = $id;";
	$records = mysqli_query($conn, $sql); 
	$count = mysqli_num_rows($records);  
    }else{
	echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"> Error invalid student number.
	    <button type="button" class="btn-close" 
	    data-dismiss="alert" aria-label="Close">  
	</button></div>';
    }
}//end if    
?>
<!doctype html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8">  
    <meta name="viewport" content="width=device-width, initial-scale=1,shrink-to-fit=no"> 
    <!-- Bootstrap CSS -->  
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

</head> 

<body style="margin:40px"> 

<?php 
// SHOW bootstrap alert !
if($showAlert) {
    echo ' <div class="alert alert-success  
	alert-dismissible fade show" role="alert"> 

	<strong>Success!</strong> Your ligne is  
	now created and check this out.  
	    <button type="button" class="btn-close" 
	    data-dismiss="alert" aria-label="Close">  
	</button>  
	</div> ';  
} 
if($showError) { 
    echo ' <div class="alert alert-danger  
	alert-dismissible fade show" role="alert">  
	<strong>Error!</strong> '. $showError.'

	    <button type="button" class="btn-close" 
	    data-dismiss="alert" aria-label="Close">  
	</button>  
	</div> ';  
} 
if($exists) { 
    echo ' <div class="alert alert-danger  
	alert-dismissible fade show" role="alert"> 

	<strong>Error!</strong> '. $exists.'
	    <button type="button" class="btn-close" 
	    data-dismiss="alert" aria-label="Close">  
	</button> 
	</div> ';  
} 

?> 




<?php
$info = mysqli_fetch_row($userInfo);
echo "<h1 class='text-center'> Questions de l'éléve : ".$info[2].' '.$info[3].' <span class="text-muted"> N°'.$info[0].'</span></h1>';
    if($count <1){
        echo'<h2 class="text-muted text-center">Aucun question pour le moment !</h2>';
    }

     while ($course = mysqli_fetch_assoc($records)){
echo '<div class="card mb-3">';
  echo '<div class="card-header">';
    echo 'Question lors du cours "<span class="fw-bolder"> '.$course['titre'].'</span>"      <span class="badge bg-info"> n°'.$course['idMsg'].'</span> :' ;
  echo '</div>';
  echo '<div class="card-body text-center">';
    echo '<h5 class="card-title">'.$course['msg'].'</h5>';
    echo '<p class="card-text text-end">Par : '.$course['nom']. ' '.$course['prenom'].'</p>';
    echo '<a href="./rep?id='.$course['idMsg'].'" class="btn btn-primary">Répondre</a>';
 //   echo '<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>';
  echo '</div>';
  echo '<div class="card-footer text-muted">';
    echo 'cours';
  echo '</div>';
  echo '</div>';
     }
?>

<!-- JavaScript Lib for bootstrap -->  
<!-- At the end of page for loading optimisation -->

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"> 
</script> 

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"> 
</script> 

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"> 
</script>  
</body>  
</html> 

