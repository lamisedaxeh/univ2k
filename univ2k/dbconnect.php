<?php 
/* 
 * Setup var for login
 */
$servername = "localhost";  
$username = "rootdata";  
$password = ""; 

$database = "data"; 

// Create a connection  
$conn = mysqli_connect($servername, $username, $password, $database); 

if($conn) { 
   //Show alert on connection sucesss
   echo '<div class="alert alert-success" role="alert">Connection DB Success</div>';  
}  
else {
    //Show error on connection error
    die('<div class="alert alert-danger" role="alert"> Error'. mysqli_connect_error()."</div>");  
}  
?> 

